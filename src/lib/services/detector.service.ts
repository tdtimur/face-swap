import type {Tensor4D} from '@tensorflow/tfjs-core/dist/tensor'
import type {FaceDetection} from 'face-api.js'
import {draw, matchDimensions, Mtcnn, resizeResults, SsdMobilenetv1, TinyFaceDetector, TinyYolov2} from 'face-api.js';
import type {DetectedObject} from '@tensorflow-models/coco-ssd';
import {base} from '$app/paths';
import * as tf from '@tensorflow/tfjs-core';

type DetectorNetworks = SsdMobilenetv1 | TinyFaceDetector | TinyYolov2 | Mtcnn
type HTMLMediaElements = HTMLImageElement | HTMLVideoElement | HTMLCanvasElement
type FaceDetectionInput = Tensor4D | HTMLMediaElements | string
type DetectedFaces = {
  image: HTMLImageElement;
  detections: FaceDetection[];
}
export type DetectedObjects = {
    image: HTMLImageElement;
    detections: DetectedObject[];
}

export abstract class FaceDetector {
  protected abstract modelUri: string
  protected abstract neuralNet: DetectorNetworks
  protected abstract detectionThreshold: number
  protected abstract isModelLoaded: boolean

  public get getScoreThreshold (): number {
    return this.detectionThreshold
  }

  public setScoreThreshold (score: number): void {
    if (typeof score !== 'number' || score <= 0 || score > 1) {
      throw new Error('Score must be a non-negative number between 0 and 1')
    }
    this.detectionThreshold = score
  }

  public abstract detectSingleFace (input: FaceDetectionInput): Promise<FaceDetection | undefined>

  public abstract detectAllFaces (input: FaceDetectionInput): Promise<FaceDetection[]>

  public toImageData (image: HTMLImageElement): ImageData {
    const canvas = document.createElement('canvas')
    canvas.width = image.width
    canvas.height = image.height
    const ctx = canvas.getContext('2d')
    if (ctx == null) throw new Error('No canvas context found')
    ctx.drawImage(image, 0, 0)
    const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height)
    canvas.remove()
    return imageData
  }

  public crop(
      image: HTMLImageElement, width: number, height: number, x: number, y: number
  ): HTMLImageElement {
    const canvas = document.createElement('canvas')
    canvas.width = image.width
    canvas.height = image.height
    const ctx = canvas.getContext('2d')
    if (ctx == null) throw new Error('No canvas context found')
    ctx.drawImage(image, 0, 0)
    const imageData = ctx.getImageData(x, y, width, height)
    canvas.remove()
    const croppedCanvas = document.createElement('canvas')
    croppedCanvas.width = width
    croppedCanvas.height = height
    const croppedCtx = croppedCanvas.getContext('2d')
    if (croppedCtx == null) throw new Error('No canvas context found')
    croppedCtx.putImageData(imageData, 0, 0)
    const croppedImage = new Image()
    croppedImage.src = croppedCanvas.toDataURL()
    croppedCanvas.remove()
    return croppedImage
  }

  public swapFaces (
      sourceImage: HTMLImageElement,
      targetImage: HTMLImageElement,
      source: FaceDetection,
      target: FaceDetection
  ): HTMLImageElement {
    const sourceImageData = this.toImageData(sourceImage)
    const targetImageData = this.toImageData(targetImage)

    const targetCanvas = document.createElement('canvas')
    targetCanvas.width = targetImage.width
    targetCanvas.height = targetImage.height
    const targetCtx = targetCanvas.getContext('2d')
    if (targetCtx == null) throw new Error('No canvas context found')
    targetCtx.putImageData(targetImageData, 0, 0)

    const sourceCanvas = document.createElement('canvas')
    sourceCanvas.width = sourceImage.width
    sourceCanvas.height = sourceImage.height
    const sourceCtx = sourceCanvas.getContext('2d')
    if (sourceCtx == null) throw new Error('No canvas context found')
    sourceCtx.putImageData(sourceImageData, 0, 0)

    const targetWidth = target.box.width
    const targetHeight = target.box.height
    const targetX = target.box.x
    const targetY = target.box.y

    const sourceWidth = source.box.width
    const sourceHeight = source.box.height
    const sourceX = source.box.x
    const sourceY = source.box.y

    targetCtx.drawImage(sourceCanvas, sourceX, sourceY, sourceWidth, sourceHeight, targetX, targetY, targetWidth, targetHeight)

    const newTargetImage = new Image()
    newTargetImage.src = targetCanvas.toDataURL()
    sourceCanvas.remove()
    targetCanvas.remove()
    return newTargetImage
  }

  public async base64StringToFaceDetectionInput (base64Image: string): Promise<FaceDetectionInput> {
    const asFile = this.base64ToFile(base64Image)
    const objectUrl = URL.createObjectURL(asFile)
    const image = new Image()
    image.src = objectUrl
    // Wait for the image to be loaded
    return await new Promise<Tensor4D>((resolve, reject) => {
      image.onload = () => {
        const tensor = tf.browser.fromPixels(image)
        const expandedTensor = tensor.expandDims(0)
        image.remove()
        resolve(expandedTensor as Tensor4D)
      }
      image.onerror = () => {
        reject(new Error('Could not load image'))
      }
    })
  }

  public imageToBase64 (image: HTMLImageElement): string {
    const canvas = document.createElement('canvas')
    canvas.width = image.naturalWidth
    canvas.height = image.naturalHeight
    const ctx = canvas.getContext('2d')
    if (ctx == null) throw new Error('No canvas context found')
    ctx.drawImage(image, 0, 0)
    const base64Image = canvas.toDataURL('image/jpeg')
    canvas.remove()
    return base64Image
  }

  public base64ToFile (base64Image: string): File {
    if (!base64Image.startsWith('data:image/')) {
      throw new Error('Base64 string is not an image')
    }
    const byteString = atob(base64Image.split(',')[1])
    const mimeString = base64Image.split(',')[0].split(':')[1].split(';')[0]
    const ab = new ArrayBuffer(byteString.length)
    const ia = new Uint8Array(ab)
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i)
    }
    const blob = new Blob([ab], { type: mimeString })
    return new File([blob], 'image.jpg', { type: mimeString })
  }

  public async loadImageFromUrl (url: string): Promise<HTMLImageElement> {
    const image = new Image()
    image.crossOrigin = 'anonymous'
    image.src = url
    return await new Promise<HTMLImageElement>((resolve, reject) => {
      image.onload = () => {
        resolve(image)
      }
      image.onerror = () => {
        reject(new Error('Could not load image'))
      }
    })
  }

  protected abstract loadModel (): Promise<void>
}

export class FaceDetectorSSDMobileNetV1 extends FaceDetector {
  protected modelUri = `${base}/models/ssd_mobilenetv1_model-weights_manifest.json`;
  protected neuralNet = new SsdMobilenetv1()
  protected detectionThreshold = 0.7
  protected isModelLoaded: boolean;
  // protected isSsdModelLoaded: boolean;
  // protected cocoSsdModel: ObjectDetection | undefined;

  public constructor () {
    super()
    this.isModelLoaded = false
    // this.isSsdModelLoaded = false
  }

  public async detectSingleFace (input: FaceDetectionInput): Promise<FaceDetection | undefined> {
    const detections = await this.detectAllFaces(input)
    if (detections.length === 0) return undefined
    return detections[0] // Detection with the highest confidence score
  }

  public async detectAllFaces (input: FaceDetectionInput): Promise<FaceDetection[]> {
    await this.loadModel()
    const detectionOptions = { minConfidence: this.getScoreThreshold }
    type NoTensor = Exclude<FaceDetectionInput, Tensor4D>
    return await this.neuralNet.locateFaces(input as NoTensor, detectionOptions)
  }

  public async detectFacesWithBoundingBox(
      image: HTMLImageElement,
      threshold: number,
      nFaces: number,
      drawBoxes = false,
  ): Promise<DetectedFaces> {
    if (image.width === 0 || image.height === 0) return { image, detections: [] };
    await this.loadModel()
    this.setScoreThreshold(threshold)
    const displaySize = { width: image.width, height: image.height }
    const canvas = document.createElement('canvas')
    matchDimensions(canvas, displaySize)
    const ctx = canvas.getContext('2d')
    if (ctx == null) throw new Error('No canvas context found')
    ctx.drawImage(image, 0, 0)
    const detectionOptions = { minConfidence: this.getScoreThreshold }
    const allDetections = await this.neuralNet.locateFaces(image, detectionOptions)
    if (allDetections.length === 0) return { image, detections: [] };
    const detections = allDetections.slice(0, nFaces)
    const resizedDetections = resizeResults(detections, displaySize)
    drawBoxes && draw.drawDetections(canvas, resizedDetections)
    const dataUrl = canvas.toDataURL()
    const asFile = await this.base64ToFile(dataUrl)
    const objectUrl = URL.createObjectURL(asFile)
    const resImg = await this.loadImageFromUrl(objectUrl)
    return { image: resImg, detections: resizedDetections }
  }

  public async swapV2(
      host: string,
      apiKey: string,
      source: HTMLImageElement,
      targetFace: DetectedObject,
      target: HTMLImageElement,
      threshold?: number,
  ): Promise<HTMLImageElement> {
    if (!threshold) threshold = 0.6;
    const targetFaceCropped = this.crop(
        target,
        targetFace.bbox[2],
        targetFace.bbox[3],
        targetFace.bbox[0],
        targetFace.bbox[1],
    );
    const sourceBase64 = this.imageToBase64(source);
    const targetFaceBase64 = this.imageToBase64(targetFaceCropped);
    const targetBase64 = this.imageToBase64(target);
    const response = await fetch(`${host}/swap`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': apiKey,
      },
        body: JSON.stringify({
          source: sourceBase64,
          target_face: targetFaceBase64,
          target: targetBase64,
          similarity_threshold: threshold,
        }),
    }).then(res => res.blob());
    const objectUrl = URL.createObjectURL(response);
    return await this.loadImageFromUrl(objectUrl);
  }

  public async filterPersonsWithFace(input: DetectedObjects): Promise<DetectedObject[]> {
    type DetectedWithFace = {
        person: DetectedObject;
        cropped: HTMLImageElement;
        face?: FaceDetection;
    }
    const crop = this.crop;
    const faces: DetectedWithFace[] = await Promise.all(
        input.detections.map(async (d) => {
          const cropped = crop(input.image, d.bbox[2], d.bbox[3], d.bbox[0], d.bbox[1]);
          const face = await this.detectSingleFace(cropped);
          return { person: d, cropped, face };
        })
    );
    return faces.filter(f => f.face !== undefined).map(f => f.person);
  }

  public async runDemo (url: string): Promise<void> {
    const image = await this.loadImageFromUrl(url)
    const displaySize = { width: image.width, height: image.height }
    const canvas = document.createElement('canvas')
    matchDimensions(canvas, displaySize)
    const ctx = canvas.getContext('2d')
    if (ctx == null) throw new Error('No canvas context found')
    ctx.drawImage(image, 0, 0)

    const detections = await this.detectAllFaces(image)
    const resizedDetections = resizeResults(detections, displaySize)
    draw.drawDetections(canvas, resizedDetections)
    // Get object url from canvas
    const dataUrl = canvas.toDataURL()
    const asFile = await this.base64ToFile(dataUrl)
    const objectUrl = URL.createObjectURL(asFile)
    window.open(objectUrl, '_blank')
    URL.revokeObjectURL(objectUrl)
    image.remove()
    canvas.remove()
  }

  protected async loadModel (): Promise<void> {
    if (this.isModelLoaded) return
    await this.neuralNet.loadFromUri(this.modelUri)
    this.isModelLoaded = true
  }
}

const detector = new FaceDetectorSSDMobileNetV1();
export default detector;
