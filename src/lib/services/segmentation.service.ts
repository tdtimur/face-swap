import type { Results } from "@mediapipe/selfie_segmentation";
import { SelfieSegmentation } from "@mediapipe/selfie_segmentation";

export type InputType = HTMLImageElement | HTMLVideoElement | HTMLCanvasElement;
export class SelfieSegmentationService {
    private readonly model: SelfieSegmentation;
    public selectedModel: number = 0;
    public selfieMode = false;
    public activeEffect: 'mask' | 'both' | 'fore' = 'fore';

    public constructor() {
      this.model = new SelfieSegmentation({locateFile: (file: string) => {
        console.log(`https://cdn.jsdelivr.net/npm/@mediapipe/selfie_segmentation/${file}`)
        return `https://cdn.jsdelivr.net/npm/@mediapipe/selfie_segmentation/${file}`;
      }});
      console.log(this.model)
      this.setOptions();
    }

    public set selfie(value: boolean) {
      this.selfieMode = value;
      this.setOptions();
    }

    public set modelSelection(value: number) {
      this.selectedModel = value;
      this.setOptions();
    }

    public set effect(value: 'mask' | 'both' | 'fore') {
      this.activeEffect = value;
    }

    public setOptions(): void {
      this.model.setOptions({
          selfieMode: this.selfieMode,
          modelSelection: 0,
        });
    }

    public async run(
      input: InputType,
      output: HTMLCanvasElement,
      foregroundFill = 'rgba(0, 255, 0, 1)',
      backgroundFill = 'rgba(0, 255, 0, 1)',
    ): Promise<void> {
      const canvasCtx = output.getContext('2d');
      if (canvasCtx == null) throw new Error('No canvas context found');

      const resultCallback = (results: Results): void => {
        canvasCtx.save();
        canvasCtx.clearRect(0, 0, output.width, output.height);
        canvasCtx.drawImage(results.segmentationMask, 0, 0, output.width, output.height);

        if (this.activeEffect === 'mask' || this.activeEffect === 'both') {
          canvasCtx.globalCompositeOperation = 'source-in';
          // This can be a color or a texture or whatever...
          canvasCtx.fillStyle = backgroundFill;
          canvasCtx.fillRect(0, 0, output.width, output.height);
        } else {
          canvasCtx.globalCompositeOperation = 'source-out';
          canvasCtx.fillStyle = foregroundFill;
          canvasCtx.fillRect(0, 0, output.width, output.height);
        }

        canvasCtx.globalCompositeOperation = 'destination-atop';
        canvasCtx.drawImage(results.image, 0, 0, output.width, output.height);

        canvasCtx.restore();
      }

      this.model.onResults(resultCallback);
      await this.model.send({ image: input });
    }
}
