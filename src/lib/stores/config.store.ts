import { writable } from 'svelte/store';

export const detectionThreshold = writable<number>(Number(localStorage.getItem('detectionThreshold') || 0.5));
export const nFaces = writable<number>(Number(localStorage.getItem('nFaces') || 5));

detectionThreshold.subscribe((value) => {
    localStorage.detectionThreshold = value;
});

nFaces.subscribe((value) => {
   localStorage.nFaces = value;
});
