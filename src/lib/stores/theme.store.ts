import { writable } from "svelte/store";

export const isLightTheme = writable<boolean>(JSON.parse(localStorage.getItem("isLightTheme") ?? "false"));

isLightTheme.subscribe((value) => {
    localStorage.isLightTheme = JSON.stringify(value);
})
