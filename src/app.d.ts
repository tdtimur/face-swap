// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
type CocoSsd = typeof import('@tensorflow-models/coco-ssd');
type ObjectDetection = import('@tensorflow-models/coco-ssd').ObjectDetection;

interface Window {
	SelfieSegmentation: (config) => SelfieSegmentation;
	Camera: (input, config) => Camera;
	cocoSsd: CocoSsd;
	cocoSsdModel: ObjectDetection | undefined;
}

declare global {
	namespace App {
		// interface Error {}
		// interface Locals {}
		// interface PageData {}
		// interface Platform {}
	}
}
